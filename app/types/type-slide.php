<?php

use PostTypes\PostType;

$options = [
	'exclude_from_search' => true,
	'publicly_queryable' => false,
	'show_ui' => true,
	'show_in_nav_menus' => false,
	'show_in_menu' => true,
	'show_in_admin_bar' => true,
	'menu_position' => 25,
	'capability_type' => 'page',
	'hierarchical' => false,
	'supports' => [ 'title', 'author', 'page-attributes', 'thumbnail' ],
	'has_archive' => false
];

$labels = [
	'name' => __( 'Slides', 'flickity-slider' ),
	'singular_name' => __( 'Slide', 'flickity-slider' ),
	'add_new' => __( 'Add New', 'flickity-slider' ),
	'add_new_item' => __( 'Add New Slide', 'flickity-slider' ),
	'edit_item' => __( 'Edit Slide', 'flickity-slider' ),
	'new_item' => __( 'New Slide', 'flickity-slider' ),
	'view_item' => __( 'View Slide', 'flickity-slider' ),
	'view_items' => __( 'View Slides', 'flickity-slider' ),
	'search_items' => __( 'Search Slides', 'flickity-slider' ),
	'not_found' => __( 'No Slides found', 'flickity-slider' ),
	'all_items' => __( 'All Slides', 'flickity-slider' ),
	'attributes' => __( 'Slide Order', 'flickity-slider' ),
	'featured_image' => __( 'Slide Image', 'flickity-slider' ),
	'set_featured_image' => __( 'Set slide image', 'flickity-slider' ),
	'remove_featured_image' => __( 'Remove slide image', 'flickity-slider' ),
	'use_featured_image' => __( 'Use as slide image', 'flickity-slider' ),
	'menu_name' => __( 'Slides', 'flickity-slider' )
];

$slide = new PostType( 'slide', $options, $labels );

$slide->icon('dashicons-images-alt2');

$slide->columns()->add( [
	'thumbnail' => __( 'Image', 'flickity-slider' ),
] );

$slide->columns()->set( [
	'cb' => '<input type="checkbox" />',
	'thumbnail' => __( 'Image', 'flickity-slider' ),
	'title' => __( 'Title', 'flickity-slider' ),
	'order' => __( 'Order', 'flickity-slider' ),
	'author' => __( 'Author', 'flickity-slider' ),
	'date' => __( 'Date', 'flickity-slider' ),
] );

$slide->columns()->populate( 'thumbnail', function ( $column, $post_id ) {
	echo '<a href="' . get_edit_post_link( $post_id ) . '"><span class="media-icon image-icon">' . get_the_post_thumbnail( $post_id, array( 60, 60 ), array( 'class' => 'alignleft' ) ) . '</span></a>';
} );

$slide->columns()->populate( 'order', function ( $column, $post_id ) {
	global $post;
	echo $post->menu_order;
} );

$slide->columns()->sortable( [
	'order'  => [ 'menu_order', true ],
] );

$slide->register(); 
