<?php

namespace HV\FlickitySlider;

// Set up plugin class
class Init {

  function __construct() {

    // Include all post types
    foreach (glob(FLICKITY_SLIDER_PLUGIN_DIR . 'app/types/*.php') as $filename) {
      include $filename;
      
    }

    // Enqueue scripts and styles
    add_action( 'wp_enqueue_scripts', array( $this, 'flickity_slider_scripts' ) );
  }

  public function flickity_slider_scripts() {

    wp_register_style( 'flickity-slider-style', FLICKITY_SLIDER_PLUGIN_URL . 'assets/styles/flickity.min.css', '', '2.1.2' );
    wp_enqueue_style( 'flickity-slider-style' );
    wp_enqueue_script( 'flickity-slider-script', FLICKITY_SLIDER_PLUGIN_URL . 'assets/scripts/flickity.pkgd.min.js', array( 'jquery' ), '2.1.2', true );
  }
}
