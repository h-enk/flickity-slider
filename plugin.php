<?php
/*
Plugin Name: Flickity Slider
Plugin URI: https://gitlab.com/h-enk/flickity-slider
Description: Flickity Slider plugin for WordPress.
Version: 1.0.0
Author: Henk Verlinde
Author URI: https://henkverlinde.com/
License: MIT License
Text Domain: flickity-slider
Domain Path: /lang
*/

namespace HV\FlickitySlider;

// Set up autoloader
require __DIR__ . '/vendor/autoload.php';

// Define Constants
define( 'FLICKITY_SLIDER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'FLICKITY_SLIDER_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

// Autoload Init class
$flickity_slider_init = new Init();

// Load plugin textdomain
function flickity_slider_load_textdomain() {
  load_plugin_textdomain( 'flickity-slider', false, basename( dirname( __FILE__ ) ) . '/lang' );
}
add_action( 'init', __NAMESPACE__ . '\\flickity_slider_load_textdomain' );
