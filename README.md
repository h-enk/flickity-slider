# Slides
Slides plugin for WordPress.

## Requirements
* PHP >= 5.3.2

## Features
* Adds Slide custom post type (admin-only)
* Adds [Flickity](https://github.com/metafizzy/flickity) CSS and JavaScript

## Based on
* [Roots Example CPT](https://github.com/MWDelaney/roots-example-cpt)
* [PostTypes](https://github.com/jjgrainger/PostTypes)
