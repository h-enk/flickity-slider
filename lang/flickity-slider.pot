#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Board Members\n"
"POT-Creation-Date: 2018-11-14 11:21+0100\n"
"PO-Revision-Date: 2018-11-09 08:58+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: plugin.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: app/types/type-slide.php:20 app/types/type-slide.php:36
msgid "Slides"
msgstr ""

#: app/types/type-slide.php:21
msgid "Slide"
msgstr ""

#: app/types/type-slide.php:22
msgid "Add New"
msgstr ""

#: app/types/type-slide.php:23
msgid "Add New Slide"
msgstr ""

#: app/types/type-slide.php:24
msgid "Edit Slide"
msgstr ""

#: app/types/type-slide.php:25
msgid "New Slide"
msgstr ""

#: app/types/type-slide.php:26
msgid "View Slide"
msgstr ""

#: app/types/type-slide.php:27
msgid "View Slides"
msgstr ""

#: app/types/type-slide.php:28
msgid "Search Slides"
msgstr ""

#: app/types/type-slide.php:29
msgid "No Slides found"
msgstr ""

#: app/types/type-slide.php:30
msgid "All Slides"
msgstr ""

#: app/types/type-slide.php:31
msgid "Slide Order"
msgstr ""

#: app/types/type-slide.php:32
msgid "Slide Image"
msgstr ""

#: app/types/type-slide.php:33
msgid "Set slide image"
msgstr ""

#: app/types/type-slide.php:34
msgid "Remove slide image"
msgstr ""

#: app/types/type-slide.php:35
msgid "Use as slide image"
msgstr ""

#: app/types/type-slide.php:44 app/types/type-slide.php:58
msgid "Image"
msgstr ""

#: app/types/type-slide.php:59
msgid "Title"
msgstr ""

#: app/types/type-slide.php:60
msgid "Order"
msgstr ""

#: app/types/type-slide.php:61
msgid "Author"
msgstr ""

#: app/types/type-slide.php:62
msgid "Date"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Flickity Slider"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://gitlab.com/h-enk/flickity-slider"
msgstr ""

#. Description of the plugin/theme
msgid "Flickity Slider plugin for WordPress."
msgstr ""

#. Author of the plugin/theme
msgid "Henk Verlinde"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://henkverlinde.com/"
msgstr ""
